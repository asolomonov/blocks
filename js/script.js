function addListener(element, eventName, callback) {
    if (element['addEventListener']) {
        element.addEventListener(eventName, callback, false);
    } else if (element['attachEvent']) {
        element.attachEvent('on' + eventName, function() {
            callback.call(element)
        });
    }
}

function addListeners(elements, eventName, callback) {
    for (var i in elements) {
        addListener(elements[i], eventName, callback);
    }
}

function addClass(element, className) {
    var regExp = new RegExp('(^|\\s)' + className + '(\\s|$)', 'g')
    if (regExp.test(element.className))  {
        return;
    }
    element.className = (element.className + ' ' + className).replace(/\s+/g, ' ').replace(/(^ | $)/g, '')
}

function documentReady(callback) {

    var isReady = false;
    function ready() {
        if (isReady) {
            return;
        }
        isReady = true;
        callback();
    }

    if ('complete' === document.readyState) {
        setTimeout(ready);
    } else {
        addListener(document, 'DOMContentLoaded', ready);
        addListener(window, 'load', ready);
    }
}

function findsByClass(element, classList) {
    if (element['getElementsByClassName']) {
        return element.getElementsByClassName(classList)
    } else {
        if (!element['getElementsByTagName']) {
            return null;
        }
        var list = element.getElementsByTagName('*');
        var classArray = classList.split(/\s+/);
        var result = [];
        for (var i = 0; i < list.length;++i) {
            for (var j = 0; j < classArray.length;++j) {
                if (-1 != list[i].className.search('\\b' + classArray[j] + '\\b')) {
                    result.push(list[i]);
                    break;
                }
            }
        }
        return result;
    }
}

function findByClass(element, classList) {
    var result = findsByClass(element, classList);
    return result && result.length
        ? result[0]
        : null;
}

function getAttr(element, attr) {
    var result = (element.getAttribute && element.getAttribute(attr)) || null;
    if (!result) {
        var attrs = element.attributes;
        if (!attrs) {
            return null;
        }
        var length = attrs.length;
        for (var i = 0; i < length; ++i) {
            if (attrs[i].nodeName === attr) {
                result = attrs[i].nodeValue;
            }
        }
    }
    return result;
}

function findsByAttrVal(elements, attr, value) {
    var result = [];
    for (var i in elements) {
        if (value == getAttr(elements[i], attr)) {
            result.push(elements[i]);
        }
    }
    return result;
}

function findByAttrVal(elements, attr, value) {
    var result = findsByAttrVal(elements, attr, value);
    return result.length
        ? result[0]
        : null;
}

function insertBefore(element, newElement) {
    element.parentNode.insertBefore(newElement, element);
}

function getRandomInt(min, max) {
    min = min || 0;
    max = max || 1000000000;
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var colorParts = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];

function getRandomColor() {
    var max = colorParts.length - 1;
    return '#'
        + colorParts[getRandomInt(0, max)]
        + colorParts[getRandomInt(0, max)]
        + colorParts[getRandomInt(0, max)]
        + colorParts[getRandomInt(0, max)]
        + colorParts[getRandomInt(0, max)]
        + colorParts[getRandomInt(0, max)];
}


function getStyle(element) {
    return element.currentStyle || window.getComputedStyle(element);
}

function fadeIn(element, duration, callback) {
    duration = duration || 500;
    callback = callback || function() {};
    var style = element.style;
    var opacity = 100;
    var step = 50 * 100 / duration;
    var interval = null;
    interval = setInterval(function() {
        opacity = Math.ceil(opacity - step);
        if (undefined != style['opacity']) {
            style.opacity = opacity / 100;
        } else if (undefined != style['filter']) {
            style.filter = 'alpha(opacity=' + opacity + ')';
        }
        if (0 == opacity) {
            clearInterval(interval);
            style.display = 'none';
            callback();
        }
    }, 25);
}

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires*1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(JSON.stringify(value));

    var updatedCookie = name + "=" + value;

    for(var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));

    return matches ? JSON.parse(decodeURIComponent(matches[1])) : undefined;
}

function createItem(position, content, background) {
    var item = document.createElement('div');
    item.style.background = background;
    item.innerHTML = content;
    item.setAttribute('class', 'item');
    item.setAttribute('position', position);
    item.setAttribute('content', content);
    item.setAttribute('background', background);
    return item;
}